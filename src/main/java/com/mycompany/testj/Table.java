/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testj;

public class Table {
    
    char[][] detail ={{'_','_','_'},{'_','_','_'},{'_','_','_'}};
    Player P1;
    Player P2;
    private Player currentPlayer;
    private Player winner = null;
    int lastCol;
    int lastRow;
    int countTurn = 0;

    public Table(Player P1, Player P2) {
        this.P1 = P1;
        this.P2 = P2;
        this.currentPlayer = this.P1;
    }

    public char[][] getDetail() {
        return detail;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void switchPlayer(){
        if(this.currentPlayer == P1){
            this.currentPlayer = P2;
        }else{
            this.currentPlayer = P1;
        }
        
    }

    public boolean setRowCol(int row, int col) {
       if(this.detail[row-1][col-1]!= '_'){
           return false;
       }
       this.detail[row-1][col-1] = currentPlayer.getName();
       lastCol = col-1;
       lastRow = row-1;
       countTurn ++;
       return true;
    } 
    public boolean checkRow(){
        for(int col=0; col<this.detail[lastRow].length;col++){
            if(this.detail[lastRow][col] != currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
        public boolean checkCol(){
        for(int row=0; row<this.detail.length;row++){
            if(this.detail[row][lastCol] != currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    public boolean checkX1(){
        for(int i=0; i<this.detail.length;i++){
         if(this.detail[i][i]!=currentPlayer.getName()){
             return false;
         }   
        }
        return true;
    }
    
        public boolean checkX2(){
        for(int i=0; i<this.detail.length;i++){
         if(this.detail[i][this.detail.length-i-1]!=currentPlayer.getName()){
             return false;
         }   
        }
        return true;
    }
    public void updateStat(){
        if(this.P1 == this.winner){
            this.P1.win();
            this.P2.lose();
        }else if(this.P2 == this.winner){
            this.P2.win();
            this.P1.lose();
        }else{
            this.P2.draw();
            this.P1.draw();
        }
    }
    public boolean checkWin() {
        if(checkCol() || checkRow()||checkX1() || checkX2()){
            this.winner = currentPlayer;
            updateStat();
             return true;
        }
        if(countTurn==9){
            updateStat();
            return true;
        }
       return false;
    }

    public Player getWinner() {
        return winner;
    }

    char[][] getData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
